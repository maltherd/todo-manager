from django import forms
from django.utils.timezone import now

from todo.models import Todo, TodoDetail


class TodoForm(forms.ModelForm):
    class Meta:
        model = Todo
        exclude = ['done_date', 'cancelled']
        widgets = {
            'todo_by_date': forms.DateInput(attrs={'type': 'date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs["autofocus"] = True


class TodoFinishForm(forms.ModelForm):
    class Meta:
        model = Todo
        fields = ['done_date']
        widgets = {
            'done_date': forms.DateInput(attrs={'type': 'date'})
        }

    def clean_done_date(self):
        data = self.cleaned_data['done_date'] or now()
        self.cleaned_data['done_date'] = data
        return data


TodoDetailFormSet = forms.inlineformset_factory(Todo, TodoDetail, fields=['detail_text'])
