from django.utils.timezone import now
from django.utils.html import escape
from django.shortcuts import reverse
from django.conf import settings

from todo.models import Todo, TelegramSubscription, TelegramUpdateRecord

from celery import shared_task
from datetime import timedelta
import requests


def api(method_name):
    return 'https://api.telegram.org/bot{}/{}'.format(settings.TELEGRAM_BOT_TOKEN, method_name)


@shared_task
def poll_telegram():

    last_update_id = TelegramUpdateRecord.last_update_id()
    data = {'offset': last_update_id + 1} if last_update_id else {}

    r = requests.post(api('getUpdates'), data=data)
    json = r.json()

    if not json['ok']:
        print('Error :(', json)
        return

    for update in json['result']:
        if update['update_id'] > TelegramUpdateRecord.last_update_id():
            TelegramUpdateRecord.set_last_update_id(update['update_id'])

        if 'message' in update:
            sub = None
            reply = None

            if update['message']['text'].startswith('/start'):
                sub, created = TelegramSubscription.objects.get_or_create(chat_id=update['message']['chat']['id'], who=update['message']['chat']['first_name'])
                sub.save()

                if created:
                    reply = 'You are now registered.'
                else:
                    reply = 'You were already registered.'

            elif update['message']['text'].startswith('/stop'):
                sub = TelegramSubscription.objects.filter(chat_id=update['message']['chat']['id']).first()
                if sub:
                    sub.delete()
                    reply = 'You have been unregistered.'
                else:
                    reply = 'You are not registered yet.'

            elif update['message']['text'].startswith('/alert'):
                send_telegram_alerts.delay()

            if reply:
                reply_r = requests.post(api('sendMessage'), data={'chat_id': sub.chat_id, 'text': reply, 'reply_to_message_id': update['message']['message_id']})

                reply_json = reply_r.json()
                if not reply_json['ok']:
                    print('Error :(', reply_json)


@shared_task
def send_telegram_alerts():
    urgent = Todo.objects.exclude(urgency=Todo.Urgency.NOT_URGENT)
    important = Todo.objects.exclude(importantness=Todo.Importantness.NOT_IMPORTANT)
    recent = Todo.objects.filter(creation_date__gt=now()-timedelta(days=14))

    def compact_message(todos_qs):

        def addendum(todo):
            return (
                ' (<i>{}</i>)'.format(escape(todo.get_urgency_display()))
                if not todo.is_important else
                ' (<i>{}</i>)'.format(escape(todo.get_importantness_display()))
                if not todo.is_urgent else
                ' (<i>{} and {}</i>)'.format(escape(todo.get_urgency_display()), escape(todo.get_importantness_display()))
                if todo.is_urgent and todo.is_important else
                ''
            )

        content = '\n\n'.join([
            '{}. <i><a href="{}">{}</a></i> ({} days ago){}'.format(
                index,
                '{}{}'.format(settings.TELEGRAM_BOT_DJANGO_HOST, reverse('todo:show', args=["any", todo.pk])),
                escape(todo.title),
                (now() - todo.creation_date).days,
                addendum(todo)
            )
            for index, todo in enumerate(todos_qs)
        ])

        return 'Heads up!\n\nYou have multiple tasks to do.\n\n{}'.format(content)

    def message(todo):
        base = 'Heads up!\n\nYour task <i><a href="{}">{}</a></i>\nhas been created <i>{}</i> days ago.\n\n'.format(
            '{}{}'.format(settings.TELEGRAM_BOT_DJANGO_HOST, reverse('todo:show', args=['any', todo.pk])),
            escape(todo.title),
            (now() - todo.creation_date).days
        )

        append = 'This task is {}!'.format(
            '<i>{}</i>'.format(
                escape(todo.get_urgency_display())
            ) if not todo.is_important else
            '<i>{}</i>'.format(
                escape(todo.get_importantness_display())
            ) if not todo.is_urgent else
            '<i>{}</i> and <i>{}</i>'.format(
                escape(todo.get_importantness_display()),
                escape(todo.get_urgency_display())
            )
        )

        return base + append if todo.is_important or todo.is_urgent else base

    for sub in TelegramSubscription.objects.all():
        qs = (urgent | important | recent).filter(done_date__isnull=True).filter(cancelled=False)

        r = requests.post(api('sendMessage'), data={'chat_id': sub.chat_id, 'text': compact_message(qs), 'parse_mode': 'HTML'})

        json = r.json()
        if not json['ok']:
            print('Error :(', json)
