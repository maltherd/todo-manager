from django.contrib import admin
from todo.models import *

admin.site.register(TodoList)
admin.site.register(Todo)
admin.site.register(TodoDetail)
