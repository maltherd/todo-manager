from django.db import models
from django_better_choices import choices
from django.conf import settings
from django.core.exceptions import ValidationError
from django.shortcuts import reverse
# from django.contrib.auth.hashers import check_password, make_password

from datetime import timedelta, datetime
import ics


class TodoList(models.Model):
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=127, null=True)
    order = models.IntegerField(null=True)
    url_key = models.CharField(max_length=127, unique=True)
    hex_color = models.CharField(max_length=6)

    def __str__(self):
        return f'List {self.name}'


class Todo(models.Model):
    class Importantness(models.TextChoices):
        NOT_IMPORTANT = ('0_not', 'Not important at all')
        NOT_IMPORTANT_BUT_I_SHOULD_DO_IT = ('1_i_should_do_it', 'Not -important- but i "should" do it for myself')
        IMPORTANT = ('2_important', 'Important')

    class Urgency(models.TextChoices):
        NOT_URGENT = ('0_not', 'Not urgent at all')
        GETTING_URGENT = ('1_getting_urgent', 'Getting urgent')
        URGENT = ('2_urgent', 'Urgent')

    class DialogueState(choices.BetterChoices):
        NOT_STARTED = ('0_not', 'I must start the dialogue', 'error')
        WAITING = ('1_waiting', 'It\'s on/I am waiting for responses', 'success')
        NEED_CONTINUE = ('2_need_continue', 'I must continue the dialogue/ask new questions', 'error')
        NEED_DO = ('3_need_do', 'I must do the task', 'error')
        FINISHED = ('99_finished', 'The dialogue is finished', 'success')

    title = models.CharField(max_length=256)
    creation_date = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField('TodoTag', related_name='todos', blank=True)
    related_todos = models.ManyToManyField('self', blank=True)
    done_date = models.DateTimeField(null=True, blank=True, help_text='Default is now')
    cancelled = models.BooleanField(default=False)
    urgency = models.CharField(max_length=32, choices=Urgency.choices, default=Urgency.NOT_URGENT)
    importantness = models.CharField(max_length=32, choices=Importantness.choices, default=Importantness.NOT_IMPORTANT)
    could_do = models.BooleanField(default=False)
    list = models.ForeignKey(TodoList, on_delete=models.CASCADE, null=True, blank=True)
    dialogue_state = models.CharField(max_length=32, choices=DialogueState.choices, null=True, blank=True, help_text='Leave blank if needed')
    # dialogue_person = models.ForeignKey()
    todo_by_date = models.DateTimeField(null=True, blank=True, help_text='Leave blank if needed')

    def __str__(self):
        return f'{self.title}'

    def get_dialogue_state_color(self):
        return self.DialogueState.additional_attributes_by_db_name[self.dialogue_state][0]

    @property
    def searchtext(self):
        return f'{self.title} {" ".join([detail.detail_text for detail in self.details.all()])}'

    @property
    def is_urgent(self):
        return self.urgency != self.Urgency.NOT_URGENT

    @property
    def is_important(self):
        return self.importantness != self.Importantness.NOT_IMPORTANT

    @property
    def is_dialogue(self):
        return self.dialogue_state is not None


class TodoDetail(models.Model):
    detail_text = models.CharField(max_length=256)
    todo = models.ForeignKey(Todo, on_delete=models.CASCADE, related_name='details')


class TodoTag(models.Model):
    name = models.CharField(max_length=32)


class TodoBumpEvent(models.Model):
    todo = models.ForeignKey(Todo, on_delete=models.CASCADE, related_name='bumps')
    date = models.DateTimeField(auto_now_add=True)


class TelegramSubscription(models.Model):
    chat_id = models.BigIntegerField(unique=True)  # Telegram doc says this is <52 bit
    who = models.CharField(max_length=64)


class TelegramUpdateRecord(models.Model):
    update_id = models.BigIntegerField(unique=True)

    @classmethod
    def last_update_id(cls):
        singleton = cls.objects.first()
        if singleton:
            return singleton.update_id
        else:
            return -1

    @classmethod
    def set_last_update_id(cls, update_id):
        singleton = cls.objects.first()
        if singleton:
            singleton.update_id = update_id
        else:
            singleton = cls.objects.create(update_id=update_id)
        singleton.save()


class AbstractTimetable(models.Model):
    """A set of time slots defined over an abstract week."""
    name = models.CharField(max_length=128)


class AbstractTimeSlot(models.Model):
    class Weekday(models.IntegerChoices):
        MONDAY = 0
        TUESDAY = 1
        WEDNESDAY = 2
        THURSDAY = 3
        FRIDAY = 4
        SATURDAY = 5
        SUNDAY = 6

    template = models.ForeignKey(AbstractTimetable, on_delete=models.CASCADE, related_name='time_slots')
    begin_time = models.TimeField()
    end_time = models.TimeField()
    weekday = models.IntegerField(choices=Weekday.choices)

    def clean(self):
        cleaned_data = super().clean()
        begin_time = cleaned_data['begin_time']
        end_time = cleaned_data['end_time']

        if end_time <= begin_time:
            raise ValidationError('End time must be after begin time')


class ConcreteTimeSlot(models.Model):
    monday_of_that_week = models.DateField()
    abstract_time_slot = models.ForeignKey(AbstractTimeSlot, on_delete=models.CASCADE)
    attributed_todo = models.ForeignKey(Todo, on_delete=models.CASCADE)

    @property
    def concrete_begin_time(self):
        monday_of_that_week_midnight = datetime(
            self.monday_of_that_week.year,
            self.monday_of_that_week.month,
            self.monday_of_that_week.day
        )

        return monday_of_that_week_midnight + timedelta(
            days=self.abstract_time_slot.weekday,
            hours=self.abstract_time_slot.begin_time.hour,
            minutes=self.abstract_time_slot.begin_time.minute,
        )

    @property
    def concrete_end_time(self):
        monday_of_that_week_midnight = datetime(
            self.monday_of_that_week.year,
            self.monday_of_that_week.month,
            self.monday_of_that_week.day
        )

        return monday_of_that_week_midnight + timedelta(
            days=self.abstract_time_slot.weekday,
            hours=self.abstract_time_slot.end_time.hour,
            minutes=self.abstract_time_slot.end_time.minute,
        )

    @property
    def ics_event(self):
        return ics.event.Event(
            name=self.attributed_todo.name,
            begin=self.concrete_begin_time,
            end=self.concrete_end_time,
            location='{}{}'.format(settings.ICS_DJANGO_HOST, reverse('todo:show', args=['any', self.attributed_todo.pk]))
        )


# class TodoUserAddons(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     ics_secret_token = models.CharField(max_length=128)
#
#     def set_ics_secret_token(self, new_token):
#         self.ics_secret_token = make_password(new_token)
#
#     def check_ics_secret_token(self, token):
#         return check_password(token, self.ics_secret_token)
