# Generated by Django 3.0.7 on 2020-08-16 18:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0003_auto_20200730_0257'),
    ]

    operations = [
        migrations.AddField(
            model_name='todo',
            name='done_date',
            field=models.DateTimeField(blank=True, help_text='Default is now', null=True),
        ),
    ]
