# Generated by Django 3.0.13 on 2021-03-03 16:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0007_auto_20210303_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='todo',
            name='cancelled',
            field=models.BooleanField(default=False),
        ),
    ]
