from todo.models import TodoList


def todo_lists(request):
    return {
        'todo_lists': TodoList.objects.order_by('order')
    }
