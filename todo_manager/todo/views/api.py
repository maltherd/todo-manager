from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse, Http404
from django.conf import settings

import json
import ics as ics_lib

from todo.models import Todo, ConcreteTimeSlot
from todo.views.utils import todo_list_qs


@login_required
def api_dialogue_states_for_list(request):
    qs = todo_list_qs(request.GET['list'], request.GET['all'])
    ret = {todo.pk: todo.dialogue_state for todo in qs if todo.is_dialogue}
    return JsonResponse(ret)


@login_required
def api_dialogue_states_push(request):
    data = json.loads(request.body)
    obj = get_object_or_404(Todo, pk=data['pk'])

    if request.method == 'POST':
        if obj.is_dialogue:
            obj.dialogue_state = data['dialogue_state']
            obj.save()
            return JsonResponse({'saved': True})

    return JsonResponse({'saved': False})


@login_required
def api_dialogue_states_color_map(request):
    dummy = Todo()
    x = dummy.DialogueState.additional_attributes_by_db_name
    ret = {key: value[0] for key, value in x.items()}

    return JsonResponse(ret)


def api_ics(request, token):
    if token == settings.ICS_TOKEN:
        cal = ics_lib.icalendar.Calendar()
        cal.creator = '-//maltherd//NONSGML tdm_ics//EN'
        for cts in ConcreteTimeSlot.objects.all():
            cal.add(cts.ics_event)

        # HttpResponse can consume iterators, of which cal is one
        resp = HttpResponse(cal, headers={
            'Content-Type': 'text/calendar'
        })

        return resp

    raise Http404
