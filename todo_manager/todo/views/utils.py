from django.utils.timezone import now

from datetime import timedelta

from todo.models import Todo


def todo_list_qs(list, all):
    qs = Todo.objects.order_by('-dialogue_state', '-urgency', '-importantness', 'creation_date', 'could_do').filter(done_date__isnull=True).filter(cancelled=False)

    if not all:
        qs = qs.filter(creation_date__gt=now()-timedelta(days=60))

    if list:
        qs = qs.filter(list=list)

    return qs
