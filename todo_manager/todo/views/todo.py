from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse, get_object_or_404

from functools import wraps

from todo.models import TodoList, Todo
from todo.forms import TodoForm, TodoFinishForm, TodoDetailFormSet
from todo.views.utils import todo_list_qs


def list_contextualized(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        if 'list_context' in kwargs:

            class FakeList:
                url_key = 'any'

                def __bool__(self):
                    return False

            list = FakeList() if kwargs['list_context'] == 'any' else get_object_or_404(TodoList, url_key=kwargs['list_context'])
            del kwargs['list_context']
            kwargs['list'] = list

        return f(*args, **kwargs)

    return wrapped


@login_required
@list_contextualized
def list(request, list, all=False):
    qs = todo_list_qs(list, all)
    return render(request, 'todo/todo/listnew.html', {'qs': qs, 'list': list, 'all': all})


@login_required
@list_contextualized
def new_or_edit(request, list, pk=None):
    """Credits to https://stackoverflow.com/questions/1113047/creating-a-model-and-related-models-with-inline-formsets"""

    obj = get_object_or_404(Todo, pk=pk) if pk else None

    if request.method == 'POST':
        form = TodoForm(request.POST, instance=obj)
        if form.is_valid():
            todo = form.save(commit=False)
            details_formset = TodoDetailFormSet(request.POST, instance=obj or todo)
            if details_formset.is_valid():
                form.save()
                details_formset.save()

                messages.success(request, 'Todo saved!')
                return redirect(reverse('todo:todo.show', kwargs={'pk': pk, 'list_context': list.url_key}) if pk else reverse('todo:todo.list', kwargs={'list_context': list.url_key}))

    else:
        form = TodoForm(instance=obj, initial={'list': list} if not obj else {})  # do not override pre-existing info with initial; only suggest new data.
        details_formset = TodoDetailFormSet(instance=obj or Todo())

    return render(request, 'todo/todo/new_or_edit.html', {'obj': obj, 'list': list, 'form': form, 'details_formset': details_formset})


@login_required
@list_contextualized
def show(request, list, pk):
    obj = get_object_or_404(Todo, pk=pk)
    return render(request, 'todo/todo/show.html', {'obj': obj, 'list': list})


@login_required
@list_contextualized
def delete(request, list, pk):
    obj = get_object_or_404(Todo, pk=pk) if pk else None

    if request.method == 'POST':
        obj.delete()
        messages.success(request, 'Todo deleted!')
        return redirect(reverse('todo:todo.list'), kwargs={'list_context': list.url_key})

    return render(request, 'todo/todo/delete.html', {'obj': obj, 'list': list})


@login_required
@list_contextualized
def toggle_cancel(request, list, pk):
    obj = get_object_or_404(Todo, pk=pk) if pk else None

    if request.method == 'POST':
        obj.cancelled = not obj.cancelled
        obj.save()

        messages.success(request, 'Todo cancelled!' if obj.cancelled else 'Todo un-cancelled!')
        return redirect(reverse('todo:todo.show', kwargs={'pk': pk, 'list_context': list.url_key}))

    return render(request, 'todo/todo/toggle_cancel.html', {'obj': obj, 'list': list})


@login_required
@list_contextualized
def finish(request, list, pk):
    obj = get_object_or_404(Todo, pk=pk) if pk else None

    if request.method == 'POST':
        form = TodoFinishForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()

            messages.success(request, 'Todo finished!')
            return redirect(reverse('todo:todo.list', kwargs={'list_context': list.url_key}))

    else:
        form = TodoFinishForm(instance=obj)

    return render(request, 'todo/todo/finish.html', {'obj': obj, 'form': form, 'list': list})
