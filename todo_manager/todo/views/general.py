from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse


@login_required
def index(request):
    return redirect(reverse('todo:todo.list', kwargs={'list_context': 'any'}))


@login_required
def firefox_config(request):
    return render(request, 'todo/general/firefox_config.html')
