from django.urls import path

import todo.views.api as api_views
import todo.views.timetables as timetables_views
import todo.views.todo as todo_views
import todo.views.general as general_views

app_name = 'todo'

urlpatterns = [
    path('', general_views.index, name='index'),
    path('firefox_config/', general_views.firefox_config, name='firefox_config'),

    path('abstract_timetables/', timetables_views.abstract_timetables_list, name='abstract_timetables.list'),

    path('api/ics/<str:token>', api_views.api_ics, name='api.ics'),
    path('api/dialogue_states_for_list/', api_views.api_dialogue_states_for_list, name='api.dialogue_states_for_list'),
    path('api/dialogue_states_push/', api_views.api_dialogue_states_push, name='api.dialogue_states_push'),
    path('api/dialogue_states_color_map/', api_views.api_dialogue_states_color_map, name='api.dialogue_states_color_map'),

    path('todo/<str:list_context>/', todo_views.list, name='todo.list'),
    path('todo/<str:list_context>/all', todo_views.list, {'all': True}, name='todo.all'),
    path('todo/<str:list_context>/new', todo_views.new_or_edit, name='todo.new'),
    path('todo/<str:list_context>/show/<int:pk>', todo_views.show, name='todo.show'),
    path('todo/<str:list_context>/edit/<int:pk>', todo_views.new_or_edit, name='todo.edit'),
    path('todo/<str:list_context>/delete/<int:pk>', todo_views.delete, name='todo.delete'),
    path('todo/<str:list_context>/toggle_cancel/<int:pk>', todo_views.toggle_cancel, name='todo.toggle_cancel'),
    path('todo/<str:list_context>/finish/<int:pk>', todo_views.finish, name='todo.finish'),
]
