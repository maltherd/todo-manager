class OneChoice:
    def __init__(self, my_keyword, my_db_name, my_display_name, my_attributes):
        self.keyword = my_keyword
        self.db_name = my_db_name
        self.display_name = my_display_name
        self.attributes = my_attributes


class BetterChoicesMeta(type):
    """Metaclass for simple, dumb, declarative-style choices classes. Does not support lazy-string django magic"""

    def __new__(metacls, classname, bases, classdict):
        new_dict = {}
        dumb_choices = []
        better_choices = []
        display_names = {}
        additional_attributes = {}
        display_names_by_db_name = {}
        additional_attributes_by_db_name = {}

        for keyword, tupl in classdict.items():

            if keyword.startswith('_'):
                continue

            the_choice = OneChoice(keyword, tupl[0], tupl[1], tupl[2:])

            new_dict[keyword] = the_choice

            dumb_choices.append(tupl[0:2])
            better_choices.append(tupl[0:])

            display_names[keyword] = tupl[1]
            additional_attributes[keyword] = tupl[2:]

            display_names_by_db_name[tupl[0]] = tupl[1]
            additional_attributes_by_db_name[tupl[0]] = tupl[2:]

        cls = super().__new__(metacls, classname, bases, new_dict)

        cls.choices = dumb_choices  # retrocompatibility with standard django choices
        cls.better_choices = better_choices

        cls.display_names = display_names
        cls.additional_attributes = additional_attributes

        cls.display_names_by_db_name = display_names_by_db_name
        cls.additional_attributes_by_db_name = additional_attributes_by_db_name

        return cls


class BetterChoices(metaclass=BetterChoicesMeta):
    """Just inherit from this class to benefit from the metaclass, without having to write long `metaclass=...` stuff"""
    pass
