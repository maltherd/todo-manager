import os

from celery import Celery, schedules

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

from django.conf import settings

app = Celery("todo_manager")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'send_telegram_alerts': {
        'task': 'todo.tasks.send_telegram_alerts',
        'schedule': schedules.crontab(minute=10, hour='7,13,18,21'),
        'args': [],
    },

    'poll_telegram': {
        'task': 'todo.tasks.poll_telegram',
        'schedule': 60.0,
        'args': [],
    },
}
