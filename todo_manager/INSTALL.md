* Install python >= 3.8 if you live in the past or don't have it
* Do NOT install pip with your package manager, because having a global pip makes a mess. Only use virtualenvs for your own sanity.
* Install `virtualenvwrapper` with your package manager ([debian](https://packages.debian.org/stretch/virtualenvwrapper), [arch](https://archlinux.org/packages/community/any/python-virtualenvwrapper/), [ubuntu](https://packages.ubuntu.com/groovy/virtualenvwrapper)) because it's better than doing your virtualenvs manually.
* Install `npm` with your package manager and `node` too.
* Create a virtual environment with python 3 (3.8 was used initially) : `mkvirtualenv` if python 3(.8) is the default, else `mkvirtualenv --python <path-to-python3.8>`. Pip will be installed inside it.
* In the repo's root : `pip install -r requirements.txt`
* In the `todo_manager` folder : `npm install`
* To compile the scss, call `npx gulp build` in the `todo_manager` folder (if any error happens, just `npm install` again, maybe delete node_modules/)
