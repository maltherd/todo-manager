const gulp = require("gulp");
const { parallel } = require("gulp");
const sass = require('gulp-sass')(require('node-sass'));
const cleancss = require('gulp-clean-css');
const csscomb = require('gulp-csscomb');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');

function build() {
  return gulp
    .src('./app/scss/custom-spectre.scss')
    .pipe(sass({outputStyle: 'compact', precision: 10})
      .on('error', sass.logError)
    )
    .pipe(autoprefixer())
    .pipe(csscomb())
    .pipe(gulp.dest('./app/static/css/custom'))
    .pipe(cleancss())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./app/static/css/custom'));
}

exports.build = build;
exports.default = build;
