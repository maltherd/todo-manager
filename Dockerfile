FROM python:3.12-alpine

COPY . /app/
WORKDIR /app/

RUN pip install -r requirements.txt
RUN pip install uvicorn

RUN apk add --no-cache bash
RUN apk add --no-cache npm

WORKDIR /app/todo_manager
RUN npm install --omit=dev

WORKDIR /app/
CMD ["bash", "./docker-entrypoint.sh"]
