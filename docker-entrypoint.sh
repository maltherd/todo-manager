#!/bin/bash

cd todo_manager

python manage.py migrate --noinput
python manage.py collectstatic --noinput

uvicorn --host 0.0.0.0 --port 80 app.asgi:application
